# Dockerfile For Production Containers

Since the angular development container is relatively large in size, and the frontend container is just a container that is going to serve static assets (HTML, CSS, JS), there is a way to reduce the size of this image.

```Dockerfile
FROM node:10-alpine as builder

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install

# Bundle app source
COPY . .

# Compile Static Assets
RUN $(npm bin)/ng build --prod --output-path=dist

# Copy compiled assets to nginx container
FROM nginx:alpine

COPY nginx.conf /etc/nginx/nginx.conf

WORKDIR /usr/share/nginx/html
COPY --from=builder /usr/src/app/dist/ .

EXPOSE 80
```

## Multi-stage Build

We are using multi-stage builds for our production containers to keep the size of the container image down, which enables us to do faster deployments.

We start with a Node.js-image which is based on alpine and label it `as builder`, install our dependencies, and then compile all the static assets for our frontend container. Then we copy these static assets from `builder` to an `nginx` container which is going to serve these assets for us on port 80.