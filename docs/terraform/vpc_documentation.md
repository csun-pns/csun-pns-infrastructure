# SAPS VPC Documentation

#### Notes

* Terms will appear in bold the first time they are mentioned in this documentation. They are defined in the glossary file.

* The syntax for referencing module outputs is ${module.NAME.OUTPUT}, where NAME is the module name given in the header of the module configuration block and OUTPUT is the name of the output to reference.

## What is Terraform?
Terraform is a tool for building, changing, and versioning infrastructure safely and efficiently. Terraform can manage existing and popular service providers as well as custom in-house solutions.

Configuration files describe to Terraform the components needed to run a single application or your entire datacenter. Terraform generates an execution plan describing what it will do to reach the desired state, and then executes it to build the described infrastructure. As the configuration changes, Terraform is able to determine what changed and create incremental execution plans which can be applied.

## Terraform Modules
Modules in Terraform are self-contained packages of Terraform configurations that are managed as a group. Modules are used to create reusable components in Terraform as well as for basic code organization. Modules are very easy to both use and create. The modules that we use in this infrastructure code is found in the  **Terraform Module Registry**. They are not custom.

## SAPS Virtual Private Cloud via Terraform
The code below in the first portion of this code block stores the state as a given key in a given bucket on **Amazon S3**. This backend also supports state locking and consistency checking via **Dynamo DB**. This assumes we have a bucket created called csun-saps-state-bucket. The Terraform state is written to the key found at: eip/eip.tfstate

The latter portion of code creates an **Elastic IP address** (EIP) named nat-eip. It is created for the **Network Address Translation** (NAT) service that will be employed in this VPC, specifically a **NAT gateway**.

```
    terraform {
      backend "s3" {
        bucket = "csun-saps-state-bucket"
        dynamodb_table = "csun-saps-state-lock"
        key    = "eip/eip.tfstate"
        region = "us-west-2"
      }
    }

    provider "aws" {
      region = "us-west-2"
    }

    resource "aws_eip" "nat-eip" {  
      tags{
        Name = "NAT-EIP-CSUN-SAPS"
        name = "NAT-EIP-CSUN-SAPS"
      }
    }
```

The code snippet below outputs these variables so they are accessible by other resources throughout the remote state.

```
    output "nat-eip" {
      value = "${aws_eip.nat-eip.public_ip}"
    }

    output "nat-eip-id" {
      value = "${aws_eip.nat-eip.id}"
    }
```

## State Locking with Amazon S3 and DynamoDB
The below code creates a bucket in Amazons S3 named `terraform_state_bucket` that will be used for state locking. The state locking adds a layer of protection to the s3 bucket. The bucket is created in the US West region. `versioning` is enabled to allow for state recovery in the case of accidental deletions and human error. `lifecycle` customizes the lifecycle behavior of the resource and `prevent_destroy` is set to true so that the resource is flagged and kept from being destroyed accidentally or when the destroy command is run against other resources.

The latter half of the code creates the database to be used for state locking, hence the name terraform_state_lock. The table is created with a capacity of 1 for read and write units. The attribute is given to it to specify it so that it may be inherited or overridden by another class.

```
  resource "aws_s3_bucket" "terraform_state_bucket" {
   bucket = "csun-saps-state-bucket"

   versioning {
     enabled = true
   }

   lifecycle {
     prevent_destroy = true
   }
  }

  resource "aws_dynamodb_table" "terraform_state_lock" {
   name            = "csun-saps-state-lock"
   read_capacity   = 1
   write_capacity  = 1
   hash_key        = "LockID"

   attribute {
     name = "LockID"
     type = "S"
   }
  }
```

## VPC creation via a Module
The is code block begins with creation of a other backend resource with a different key specified. The data resource code allows data to be fetched or computed for use elsewhere in a Terraform configuration; in this case its in Amazon S3.

Initially we were setting everything up without modules but found out that this was the more efficient way and the most redundant free. The module code below is a Terraform module which creates VPC resources on AWS. The parameters such as those relating to the NAT Gateway for this VPC are specified. For example, take the line of code: "single_nat_gateway = true", the Boolean value of `true` allows that particular configuration setting in the VPC to be enabled.

```
  data "terraform_remote_state" "eip" {
    backend = "s3"

    config {
      bucket = "csun-saps-state-bucket"
      region = "us-west-2"
      key = "eip/eip.tfstate"
    }
  }

  module "vpc" {
    source = "terraform-aws-modules/vpc/aws"

    name = "csun-saps-vpc-prod"
    cidr = "10.0.0.0/16"

    azs             = ["us-west-2a", "us-west-2c"]
    private_subnets = ["10.0.1.0/24", "10.0.2.0/24"]
    public_subnets  = ["10.0.101.0/24", "10.0.102.0/24"]

    enable_nat_gateway      = true
    enable_vpn_gateway      = false
    single_nat_gateway      = true
    one_nat_gateway_per_az  = false
    reuse_nat_ips           = true
    external_nat_ip_ids     = ["${data.terraform_remote_state.eip.nat-eip-id}"]

    tags = {
      Terraform = "true"
      Environment = "prod"
    }
  }
```
This module is taking care of all the route table creations and route table associations for us. The route table for the private subnets has a rule for outgoing traffic to go to the NAT Gateway and the public subnet takes all incoming traffic through the internet gateway.
All this is setup automatically and no manual configuration is needed.

## EIP Remote Data Source

The EIP remote data source in the above code snippet is needed to retrieve the ID from the Elastic IP to associate with the NAT gateway. The EIP is dedicated for the NAT Gateway and shouldn't be associated with anything else.

## Outputs
The code snippet below outputs these variables so they are accessible by other resources throughout the remote state.

```
  output "vpc_id" {
    value = "${module.vpc.vpc_id}"
  }

  output "public_subnets" {
    value = "${module.vpc.public_subnets}"
  }

  output "private_subnets" {
    value = "${module.vpc.private_subnets}"
  }
```
