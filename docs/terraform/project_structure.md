# Explaining the Project Folder Structure

## Goal of the Project Structure

Our goal in this project structure is to ensure that our code is modular and easily reusable for multiple environments.

We are targeting three environments: **Production**, **Staging** and **Development**) (prod, staging, dev).

These environments should be replicas of each other to ensure proper testing. With the use of these modules we are able to point all three environments to the same modules, ensuring parity between the environments, even when changes are made. However, this still allows us enough flexibility to allow for differences between the environments, such as the number of containers that are running or the number of servers.

However, since we are still separating the environments by different folders, we have complete control over which environments we are affecting.


# Example Project File Tree

```
.
├── dev
│   ├── eip
│   │   ├── eip.tf
│   │   └── output.tf
│   ├── remote_state
│   │   ├── remote_state.tf
│   │   ├── terraform.tfstate
│   │   └── terraform.tfstate.backup
│   └── vpc
│       └── vpc.tf
├── modules
│   ├── eip
│   │   ├── eip.tf
│   │   ├── output.tf
│   │   └── vars.tf
│   ├── remote_state
│   │   ├── output.tf
│   │   ├── s3.tf
│   │   └── vars.tf
│   └── vpc
│       ├── output.tf
│       ├── vars.tf
│       └── vpc.tf
├── prod
│   ├── eip
│   │   ├── eip.tf
│   │   └── output.tf
│   ├── remote_state
│   │   ├── remote_state.tf
│   │   ├── terraform.tfstate
│   │   └── terraform.tfstate.backup
│   └── vpc
│       └── vpc.tf
└── staging
    ├── eip
    │   ├── eip.tf
    │   └── output.tf
    ├── remote_state
    │   ├── remote_state.tf
    │   ├── terraform.tfstate
    │   └── terraform.tfstate.backup
    └── vpc
        └── vpc.tf

``` 

Each module component of the infrastructure should have a `variable.tf` and a `output.tf`. 

### variable.tf

Each `variable.tf` is reponsible to hold all the variables for the component that we are working on.

### output.tf

Each `output.tf` is reponsible to output all the specific variables and states of a project, so that they may be accessible to OTHER modules by the use of remote states.

**You can only output the variables of a module that you are pulling into your project, if the referenced module already has an output for it.**


## Remote State

The remote state component is the only component that carries its state locally, as it sets up the remote state buckets and state locking tables for the other components