# AWS Application Load Balancer (ALB)

#### Notes

* Terms will appear in bold the first time they are mentioned in this documentation. They are defined in the glossary file.

* The syntax for referencing module outputs is ${module.NAME.OUTPUT}, where NAME is the module name given in the header of the module configuration block and OUTPUT is the name of the output to reference.

* The ALB functions at Layer 7 of the OSI model.

* There will be one ALB per environment at later
stages of the project: Testing, staging, and production.

## What is an Application Load Balancer (ALB)?
An ALB is 1 of 3 types of load balancing options available from Amazon Web Services (AWS) An ALB serves as the single point of contact for clients into your network. The ALB distributes incoming application traffic across multiple targets, such as EC2 instances, or Dockerized images in our case, and in multiple Availability Zones. **This increases the availability of your application.** You need to add one or more listeners to your ALB.

**Listeners** are actively checking for incom9iong connections requests from clients, using preconfigured ports and protocols. The listener forwards this traffic to one or many predefined **target groups**.

Each target group routes traffic to registered targets such as Docker images that is specified in rules. Health checks can also be configured.

Below is the breakdown of the code that deploys the ALB within the AWS environment. All the code below is part of the ALB module. You will recall from previously in this documentation that: Modules in Terraform are self-contained packages of Terraform configurations that are managed as a group. This module creates the ALB, its respective HTTP/HTTPS listeners, target groups and health checks, security groups, and Route 53 for DNS.

## Deploy ALB within AWS Environment via Terraform
You can see the networking and security group assignments in the Terraform code below. Also specified is the load balancer type.

      resource "aws_lb" "main" {
        security_groups                   = ["${concat(var.security_group_ids, list(aws_security_group.main.id))}"]
        subnets                           = ["${var.public_subnet_ids}"]
        name                              = "alb-${var.environment}${var.name}"
        internal                          = false
        load_balancer_type                = "application"
        idle_timeout                      = 60
        enable_cross_zone_load_balancing  = true
        enable_deletion_protection        = false

In the snippet of code below the access logs are pointed to a specific bucket. More on the creation of that bucket later in this document.

        access_logs {
          bucket                          = "${var.access_log_bucket_name}"
          prefix                          = "ALB"
          enabled                         = true
        }

        timeouts {
          create                          = "10m"
          delete                          = "10m"
          update                          = "10m"
        }
      }

## Listener with SSL Certificate Attachment
The below code creates the listener. It listens for -traffic on port 80 then redirects this traffic to port 443 and attaches a SSL certificate for security. See the line of code that begins with: certificate_arn for the HTTPS block of code.

      resource "aws_alb_listener" "redirect_http_to_https" {
        load_balancer_arn   = "${aws_lb.main.arn}"
        port                = "80"
        protocol            = "HTTP"


        default_action {
          type              = "redirect"
          target_group_arn  = "${aws_alb_target_group.main.id}"

          redirect {
            port            = "443"
            protocol        = "HTTPS"
            status_code     = "HTTP_301"
          }
        }
      }

      resource "aws_alb_listener" "https" {
        load_balancer_arn   = "${aws_lb.main.arn}"
        port                = "443"
        protocol            = "HTTPS"
        certificate_arn     = "${var.ssl_certificate_arn}"

        default_action {
          type              = "forward"
          target_group_arn  = "${aws_alb_target_group.main.id}"
        }
      }

You can see the creation of the target group below along with a health check configured for HTTP.

      resource "aws_alb_target_group" "main" {
        name                  = "tg${var.environment}${var.name}"
        target_type           = "ip"
        port                  = "${var.port}"
        protocol              = "HTTP"
        vpc_id                = "${var.vpc_id}"

        health_check {
          healthy_threshold   = "2"
          interval            = "60"
          protocol            = "HTTP"
          matcher             = "200"
          timeout             = "30"
          path                = "${var.health_check_path}"
          port                = "traffic-port"
          unhealthy_threshold = "2"
          healthy_threshold   = "5"
        }

        tags {
          Name                = "tg${var.environment}${var.name}"
          Project             = "${var.project}"
          Environment         = "${var.environment}"
        }
      }

## Also in this Module
Below is additional code that is created within the ALB module. The security group and its ingress and egress rules for HTTP/HTTPS

      resource "aws_security_group" "main" {
        vpc_id = "${var.vpc_id}"

        ingress {
          from_port   = 80
          to_port     = 80
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        }

        ingress {
          from_port   = 443
          to_port     = 443
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        }

        egress {
          from_port   = 0
          to_port     = 0
          protocol    = "-1"
          cidr_blocks = ["0.0.0.0/0"]
        }

        tags {
          Name        = "sg-${var.name}-LB"
          Project     = "${var.project}"
          Environment = "${var.environment}"
        }
      }

Route 53 details for DNS.

      data "aws_route53_zone" "selected" {
        name         = "${var.domain}."
        private_zone = false
      }

      resource "aws_route53_record" "alb-record" {
        zone_id = "${data.aws_route53_zone.selected.id}" # Replace with your zone ID
        name    = "${var.environment}.${var.domain}."                     # Replace with your name/domain/subdomain
        type    = "A"

        alias {
          name                   = "${aws_lb.main.dns_name}"
          zone_id                = "${aws_lb.main.zone_id}"
          evaluate_target_health = true
        }
      }

## Outputs
The code snippet below outputs these variables so they are accessible by other resources throughout the remote state.

      output "alb_security_group_id" {
        value = "${aws_security_group.main.id}"
      }


      output "alb_target_group_id" {
        value = "${aws_alb_target_group.main.id}"
      }
