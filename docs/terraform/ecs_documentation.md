# ECS Documentation

EC2 Container Services have 4 essential parts to them that are necessary to build a container infrastructure:
- Clusters
- Services
- Tasks
- Task Definitions


# Tasks

Tasks are defined by Task Definitions. A task definition describes which docker containers to run, how much memory and cpu to use, port mappings and other types of attributes. It is important to note that a task is not simply a containers but rather defines how a container or multiple containers are run together.

Here is our task definition as of now:

```
[{
  "name": "app",
  "image": "563345143758.dkr.ecr.us-west-2.amazonaws.com/saps-prod:latest",
  "essential": true,
  "portMappings": [
    {
      "containerPort": 3000,
      "hostPort": 3000,
      "protocol": "tcp"
    }
  ]
}]
```
`"essential": true` : "If the essential parameter of a container is marked as true, and that container fails or stops for any reason, all other containers that are part of the task are stopped." [(Source)](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html)


Here is how we have implemented the task definition resource in the terraform code. We have specifically not included the the task definition resource in the module because a lot of the parameters change anyways depending on the stage. We might potentially move this into the module if we see enough of a reason to do so.

```
resource "aws_ecs_task_definition" "app" {
  family                    = "application-stack"
  network_mode              = "awsvpc"
  requires_compatibilities  = ["FARGATE"]
  
  container_definitions     = "${file("task_definitions/test-prod.json")}"
  
  task_role_arn             = "${data.aws_iam_role.ecsTaskExecutionRole.arn}"
  execution_role_arn        = "${data.aws_iam_role.ecsTaskExecutionRole.arn}"
  
  cpu                       = 256
  memory                    = 512
  
  lifecycle {
    create_before_destroy   = true
  }
}
```

It is important to note the `task_role_arn` attribute. We have attached a IAM role to the task definition. We need this IAM role so that the task definition can pull the images that are defined in the container-definitions and execute them.

# Services

Services contain tasks, and can define how many replicas of a task to run.

```
resource "aws_ecs_service" "main" {
  name                = "${var.name}"
  cluster             = "${var.cluster_id}"
  task_definition     = "${var.task_definition_id}"
  desired_count       = "${var.desired_count}"
  launch_type         = "FARGATE" 

  load_balancer {
    target_group_arn  = "${var.target_group_id}"
    container_name    = "${var.name}"
    container_port    = 3000
  }

  network_configuration {
    security_groups   = ["${var.security_group_id}"]
    assign_public_ip  = false
    subnets           = ["${var.private_subnet_ids}"]
  }
}
```
The ECS service also specifies which load balancer to attach the tasks to and what the network configurations for the service are.

We're setting the `container_port` to `3000` in the `load_balancer` attributes because that is the port where the application is running on.

# Clusters

ECS Clusters are groupings of services/tasks. They make sure that, services and tasks are isoloted from other container infrastructures withtin the same account. 

Here is our Cluster module in terraform: 

```
resource "aws_ecs_cluster" "main" {
  name = "${var.cluster_name}"

  tags{
      Name = "${var.cluster_name}"
  }
}
```

And here is how we call it:

```
module "ecs_cluster" {
  source = "../../modules/ecs_cluster"

  cluster_name = "saps-prod-cluster"
}
```

As mentioned in the [project structure documentation](project_structure.md), we have put even a small resource such as the ecs cluster into a module, to ensure that future changes to the ecs cluster are reflected in all the environments.


# Essential Data Sources

```
data "terraform_remote_state" "ecs_cluster" {
  backend = "s3"

  config {
    bucket = "saps-prod-state-bucket"
    region = "us-west-2"
    key = "ecs/ecs_cluster.tfstate"
  }
}
```
Needed to attach the services with the correct cluster


```
data "terraform_remote_state" "alb" {
  backend = "s3"

  config {
    bucket = "saps-prod-state-bucket"
    region = "us-west-2"
    key = "alb/alb.tfstate"
  }
}
```
Needed to attach tasks to the correct target-group


```
data "aws_iam_role" "ecsTaskExecutionRole" {
    name = "ecsTaskExecutionRole"
}
```
Needed to give the service the correct right to pull and execute the container images.




