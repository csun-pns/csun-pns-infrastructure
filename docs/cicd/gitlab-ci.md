# How We CI/CD

### .gitlab-ci.yml
```YML
cache:
  paths:
  - node_modules/

services:
  - docker:dind

stages:
  - build-backend
  - build-frontend
  - deploy-staging
  - deploy-prod

build-backend:
  image: docker:latest
  stage: build-backend
  script:
    - apk add --no-cache curl jq python py-pip docker
    - pip install awscli
    - $(aws ecr get-login --no-include-email --region $AWS_DEFAULT_REGION)
    - docker build -t $REPOSITORY_URL:backend ./backend
    - docker push $REPOSITORY_URL:backend
  only:
    refs:
      - master
build-frontend:
  image: docker:latest
  stage: build-frontend
  script:
    - apk add --no-cache curl jq python py-pip docker
    - pip install awscli
    - $(aws ecr get-login --no-include-email --region $AWS_DEFAULT_REGION)
    - docker build -t $REPOSITORY_URL:frontend ./frontend
    - docker push $REPOSITORY_URL:frontend
  only:
    refs:
      - master
deploy-staging:
  stage: deploy-staging
  image: docker:latest
  script:
    - apk add --no-cache curl jq python py-pip
    - pip install awscli
    - aws ecs update-service --cluster saps-staging-cluster --service app --force-new-deployment
  only:
    refs:
      - master
deploy-prod:
  stage: deploy-prod
  image: docker:latest
  script:
    - apk add --no-cache curl jq python py-pip
    - pip install awscli
    - aws ecs update-service --cluster saps-prod-cluster --service app --force-new-deployment
  only:
    refs:
      - master
  when: manual
```

# Stages

We have four stages:
* build-backend
* build-frontend
* deploy-staging
* deploy-prod


## build-*
Build-backend and build-frontend are basically the same, except with a different build context.

We start with a docker image, install the AWS cli and docker cli, log into our ECR, build the image, and push it to the ECR. This however, only happens on the master branch!

```YML
build-backend:
  image: docker:latest
  stage: build-backend
  script:
    - apk add --no-cache curl jq python py-pip docker
    - pip install awscli
    - $(aws ecr get-login --no-include-email --region $AWS_DEFAULT_REGION)
    - docker build -t $REPOSITORY_URL:backend ./backend
    - docker push $REPOSITORY_URL:backend
  only:
    refs:
      - master
```

## deploy-*

deploy-prod and deploy-staging are very similar with a small addition. 

We start off with docker container, install the aws cli and then use this command to update our service.

```
aws ecs update-service --cluster saps-prod-cluster --service app --force-new-deployment
```
Between staging and production only the cluster name changes.

`--force-new-deployment` is curcial as it will force the service to spin up new containers with the latest images.

The big difference between `deploy-staging` and `deploy-prod` is the `when: manual`. This is necessary so that we can manually trigger the changes for production, while staging gets automatically updated.

```YML
deploy-prod:
  stage: deploy-prod
  image: docker:latest
  script:
    - apk add --no-cache curl jq python py-pip
    - pip install awscli
    - aws ecs update-service --cluster saps-prod-cluster --service app --force-new-deployment
  only:
    refs:
      - master
  when: manual
```

