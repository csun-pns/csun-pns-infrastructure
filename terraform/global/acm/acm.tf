terraform {
  backend "s3" {
    bucket = "saps-prod-state-bucket"
    dynamodb_table = "saps-prod-state-lock"
    key    = "acm/acm.tfstate"
    region = "us-west-2"
  }
}

provider "aws" {
  region  = "us-west-2"
}

resource "aws_acm_certificate" "cert" {
  domain_name = "*.csun.xyz"
  validation_method = "DNS"

  tags {
    Environment = "test"
  }

  lifecycle {
    create_before_destroy = true
  }
}

data "aws_route53_zone" "zone" {
  name         = "csun.xyz."
  private_zone = false
}

resource "aws_route53_record" "cert_validation" {
  name    = "${aws_acm_certificate.cert.domain_validation_options.0.resource_record_name}"
  type    = "${aws_acm_certificate.cert.domain_validation_options.0.resource_record_type}"
  zone_id = "${data.aws_route53_zone.zone.id}"
  records = ["${aws_acm_certificate.cert.domain_validation_options.0.resource_record_value}"]
  ttl     = 60
}
