provider "aws" {
  region  = "us-west-2"
}

module "remote_state" {
  source = "../../modules/remote_state"

  bucket_name = "saps-staging-state-bucket"
  table_name = "saps-staging-state-lock"
}
