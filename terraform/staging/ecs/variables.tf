variable "project" {
  default = "saps-staging"
}

variable "environment" {
  default = "staging"
}
