terraform {
  backend "s3" {
    bucket = "saps-staging-state-bucket"
    dynamodb_table = "saps-staging-state-lock"
    key    = "vpc/vpc.tfstate"
    encrypt = true
    region = "us-west-2"
  }
}

provider "aws" {
  region  = "us-west-2"
}

module "vpc" {
  source = "../../modules/vpc"
  
  vpc_name = "saps-vpc-staging"
  vpc_env = "staging"
  remote_bucket_name = "saps-staging-state-bucket"
}