data "aws_ami" "amazon_linux_ecs" {
    most_recent = true
    
    owners = ["amazon"]

    filter {
        name    = "owner-alias"
        values  = ["amazon"]
    }

    filter {
        name = "name"
        values = ["amzn-ami-*-amazon-ecs-optimized"]
    }
}

data "aws_iam_role" "ecsTaskExecutionRole" {
  name                = "ecsTaskExecutionRole"
}

data "aws_iam_policy_document" "ecs-service-policy" {
    statement {
        actions = ["sts:AssumeRole"]

        principals {
            type = "Service"
            identifiers = ["ecs.amazonaws.com"]
        }
    }

}

data "aws_iam_policy_document" "ecs-instance-policy" {
    statement {
        actions = ["sts:AssumeRole"]

        principals {
            type    = "Service"
            identifiers = ["ec2.amazonaws.com"]
        }
    }
}