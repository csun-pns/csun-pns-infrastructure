output "name" {
  value = "${aws_ecs_service.main.name}"
}

output "id" {
  value = "${aws_ecs_service.main.id}"
}

output "cluster" {
  value = "${aws_ecs_service.main.cluster}"
}

