resource "aws_lb" "main" {
  security_groups                   = ["${concat(var.security_group_ids, list(aws_security_group.main.id))}"]
  subnets                           = ["${var.public_subnet_ids}"]
  name                              = "alb-${var.environment}${var.name}"
  internal                          = false
  load_balancer_type                = "application"
  idle_timeout                      = 60
  enable_cross_zone_load_balancing  = true
  enable_deletion_protection        = false

  timeouts {
    create                          = "10m"
    delete                          = "10m"
    update                          = "10m"
  }
}

resource "aws_alb_listener" "redirect_http_to_https" {
  load_balancer_arn   = "${aws_lb.main.arn}"
  port                = "80"
  protocol            = "HTTP"


  default_action {
    type              = "redirect"
    target_group_arn  = "${aws_alb_target_group.main.id}"

    redirect {
      port            = "443"
      protocol        = "HTTPS"
      status_code     = "HTTP_301"
    }
  }
}

resource "aws_alb_listener" "https" {
  load_balancer_arn   = "${aws_lb.main.arn}"
  port                = "443"
  protocol            = "HTTPS"
  certificate_arn     = "${var.ssl_certificate_arn}"

  default_action {
    type              = "forward"
    target_group_arn  = "${aws_alb_target_group.main.id}"
  }
}

resource "aws_alb_target_group" "main" {
  name                  = "tg${var.environment}${var.name}"
  target_type           = "instance"
  port                  = "${var.port}"
  protocol              = "HTTP"
  vpc_id                = "${var.vpc_id}"

  health_check {
    healthy_threshold   = "2"
    interval            = "60"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "30"
    path                = "${var.health_check_path}"
    port                = "traffic-port"
    unhealthy_threshold = "2"
    healthy_threshold   = "5"
  }

  tags {
    Name                = "tg${var.environment}${var.name}"
    Project             = "${var.project}"
    Environment         = "${var.environment}"
  }
}

resource "aws_security_group" "main" {
  vpc_id = "${var.vpc_id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name        = "sg-${var.name}-LB"
    Project     = "${var.project}"
    Environment = "${var.environment}"
  }
}


data "aws_route53_zone" "selected" {
  name         = "${var.domain}."
  private_zone = false
}

resource "aws_route53_record" "alb-record" {
  zone_id = "${data.aws_route53_zone.selected.id}"
  name    = "${var.environment}.${var.domain}."
  type    = "A"

  alias {
    name                   = "${aws_lb.main.dns_name}"
    zone_id                = "${aws_lb.main.zone_id}"
    evaluate_target_health = true
  }
}

