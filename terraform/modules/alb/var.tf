variable "project" {
  default = "Unknown"
}

variable "environment" {
  default = "Unknown"
}


variable "access_log_bucket_name" {}

variable "vpc_id" {}

variable "name" {}

variable "security_group_ids" {
  type = "list"
  default = []
}

variable "public_subnet_ids" {
  type = "list"
}


variable "health_check_path" {
  default = "/"
}

variable "port" {}

variable "ssl_certificate_arn" {}


variable "domain" {}
