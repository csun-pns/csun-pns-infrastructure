resource "aws_eip" "nat-eip" {  
  vpc = true
  tags{
    Name = "${var.nat-eip-name}"
  }
}
