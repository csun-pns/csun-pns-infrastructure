output "nat-eip" {
  value = "${aws_eip.nat-eip.public_ip}"
}

output "nat-eip-id" {
  value = "${aws_eip.nat-eip.id}"
}