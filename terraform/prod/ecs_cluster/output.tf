output "cluster_arn" {
  value = "${module.ecs_cluster.cluster_arn}"
}

output "cluster_id" {
  value = "${module.ecs_cluster.cluster_id}"
}
output "cluster_name" {
  value = "${module.ecs_cluster.cluster_name}"
}
