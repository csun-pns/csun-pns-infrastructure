terraform {
  backend "s3" {
    bucket = "saps-prod-state-bucket"
    dynamodb_table = "saps-prod-state-lock"
    key    = "ecs/ecs_cluster.tfstate"
    encrypt = true
    region = "us-west-2"
  }
}

provider "aws" {
  region  = "us-west-2"
}

module "ecs_cluster" {
  source = "../../modules/ecs_cluster"

  cluster_name = "saps-prod-cluster"
}
