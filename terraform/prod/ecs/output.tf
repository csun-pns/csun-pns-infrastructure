output "name" {
  value = "${module.app-service.name}"
}

output "id" {
  value = "${module.app-service.id}"
}

output "cluster" {
  value = "${module.app-service.cluster}"
}

