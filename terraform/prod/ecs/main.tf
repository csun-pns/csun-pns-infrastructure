terraform {
  backend "s3" {
    bucket          = "saps-prod-state-bucket"
    dynamodb_table  = "saps-prod-state-lock"
    key             = "ecs/ecs.tfstate"
    region          = "us-west-2"
    encrypt = true
  }
}

provider "aws" {
  region  = "us-west-2"
}

module "app-service" {
  source              = "../../modules/ecs"

  name                = "frontend"

  environment         = "prod"
  
  private_subnet_ids  = "${data.terraform_remote_state.vpc.private_subnets}"
  target_group_id     = "${data.terraform_remote_state.alb.alb_target_group_id}"
  cluster_id          = "${data.terraform_remote_state.ecs_cluster.cluster_id}"
  cluster_name        = "${data.terraform_remote_state.ecs_cluster.cluster_name}"
  vpc_id              = "${data.terraform_remote_state.vpc.vpc_id}"
  port                = 80
  desired_count       = 1
}





