output "alb_security_group_id" {
  value = "${module.alb.alb_security_group_id}"
}


output "alb_target_group_id" {
  value = "${module.alb.alb_target_group_id}"
}
