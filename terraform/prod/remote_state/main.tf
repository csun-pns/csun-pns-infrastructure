provider "aws" {
  region  = "us-west-2"
}

module "remote_state" {
  source = "../../modules/remote_state"

  bucket_name = "saps-prod-state-bucket"
  table_name = "saps-prod-state-lock"
}